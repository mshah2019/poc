﻿using System;
using Microsoft.Extensions.DependencyInjection;
using HelloWorld.Common;
namespace HelloWorld.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection()
              .AddTransient<IWriterFactory, ConsoleWriterFactory>();
            var serviceProvider = services.BuildServiceProvider();

            // resolve the dependency graph
            var appService = serviceProvider.GetService<IWriterFactory>();

            var writer=appService.CreateWriter();
            writer.Write("Hello World");

        }
    }
}
