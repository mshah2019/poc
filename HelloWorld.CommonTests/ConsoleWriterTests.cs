﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HelloWorld.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq; 
namespace HelloWorld.Common.Tests
{
    [TestClass()]
    public class ConsoleWriterTests
    {
        [TestMethod()]
        public void ConsoleWriterTest()
        {
            var mock = new Mock<IWriterFactory>();
            mock.Setup(p => p.CreateWriter()).Returns(new ConsoleWriter());
            var writer = mock.Object.CreateWriter();
            var result = writer.Write("Hello World");
            Assert.IsTrue(result==true);
        }
    }
}