﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Common
{
    public abstract class Writer : IWriter
    {
        public abstract bool Write(string message);        
        
    }
}
