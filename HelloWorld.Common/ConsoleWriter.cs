﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Common
{
    public class ConsoleWriter:Writer
    {
        public override bool Write(string message)
        {
            try
            {
                Console.WriteLine(message);
                //Console.ReadKey();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
