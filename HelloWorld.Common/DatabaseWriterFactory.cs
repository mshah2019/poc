﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Common
{
    public class DatabaseWriterFactory:IWriterFactory
    {
        public IWriter CreateWriter()
        {
            return new DatabaseWriter();
        }
    }
}
